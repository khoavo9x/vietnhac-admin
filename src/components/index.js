import CardLoading from './CardLoading'
import Heading from './Heading'
import ImageCard from './ImageCard'
import GalleryCard from './GalleryCard'
import Editor from './Editor'

export default Object.assign({}, {
  'cc-cardloading': CardLoading,
  'cc-heading': Heading,
  'cc-imagecard': ImageCard,
  'cc-gallerycard': GalleryCard,
  'cc-editor': Editor
})
