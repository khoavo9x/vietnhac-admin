export default {
  searchString (needle, string) {
    return string.toLowerCase().indexOf(needle.toLowerCase()) !== -1
  },

  generateId () {
    if (window.generatedIds === undefined) {
      window.generatedIds = []
    }

    let id = Math.random().toString(36).substr(2, 9)

    // Check duplicate
    while (window.generatedIds.indexOf(id) !== -1) {
      id = Math.random().toString(36).substr(2, 9)
    }

    window.generatedIds.push(id)

    return '_' + id
  }
}
