import enTrans from './en'
import viTrans from './vi'

export const en = enTrans
export const vi = viTrans
