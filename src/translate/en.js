export default {
  // App
  'APP.ALERT_TOKENISOUT': 'This account is logged in on another device!',

  // LoginPage
  'LOGIN.EMAIL': 'Email',
  'LOGIN.PASSWORD': 'Password',
  'LOGIN.BTN_SUBMIT': 'Login'
}
