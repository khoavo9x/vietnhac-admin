export default {
  // Common
  'CHANGE': 'Thay đổi',
  'SAVE': 'Lưu',
  'CANCEL': 'Hủy',
  'OK': 'OK',
  'ADD': 'Thêm',
  'EDIT': 'Chỉnh sửa',
  'TITLE': 'Tiêu đề',
  'INFO': 'Thông tin',
  'CONTENT': 'Nội dung',
  'PATH': 'Path',
  'IMAGES': 'Hình ảnh',
  'SAVE_SUCCESS': 'Lưu thành công!',
  'PATH_HINT': 'Lưu ý: path phải là chuỗi không dấu, không trùng với bài viết khác, các ký tự cách nhau bởi dấu gạch (-). VD: tieu-de-bai-viet',
  'DELETE_SUCCESS': 'Đã xóa!',
  'SLIDES': 'Slides',
  'AVATAR': 'Hình đại diện',
  'IMAGE_INVALID_SIZE': 'Kích thước không đúng',
  'HREF': 'Link',
  'CATEGORY': 'Chuyên mục',
  'PAGE_LINK': 'Liên kết bài viết',
  'LOGOUT': 'Đăng xuất',
  'NAME': 'Tên',
  'DESCRIPTION': 'Mô tả',
  'CLOSE': 'Đóng',
  'REF_NUMBER': 'Ref number',
  'PRICE': 'Đơn giá',
  'BRAND': 'Nhãn hiệu',
  'CAPTION': 'Caption',
  'SHORT_DESCRIPTION': 'Short description',
  'FULL_DESCRIPTION': 'Full description',
  'FEATURED': 'Nổi bật',
  'SEARCH': 'Tìm kiếm',
  'KEYWORDS': 'Từ khóa',
  'SHOW_BANNER': 'Hiển thị banner',
  'ADDRESS': 'Địa chỉ',

  // App.vue
  'APP.ALERT_TOKENISOUT': 'Tài khoản này đã bị đăng nhập bởi thiết bị khác!',

  // layouts/Login.vue
  'LOGIN.EMAIL': 'Email',
  'LOGIN.PASSWORD': 'Mật khẩu',
  'LOGIN.BTN_SUBMIT': 'Đăng nhập',

  // Sidebar
  'SIDEBAR.PRODUCT_CATEGORIES': 'Loại sản phẩm',
  'SIDEBAR.PRODUCT_BRANDS': 'Nhãn hiệu',
  'SIDEBAR.PRODUCTS': 'Sản phẩm',
  'SIDEBAR.MODULES': 'Mô-đun',
  'SIDEBAR.ORDERS': 'Đơn hàng',
  'SIDEBAR.SETTINGS': 'Cài đặt',
  'SIDEBAR.PAGES': 'Trang',

  // Toolbar
  'TOOLBAR.USER_MENU_CHANGE_PASSWORD': 'Thay đổi mật khẩu',
  'TOOLBAR.NEW_PASSWORD': 'Mật khẩu mới',

  // Product Categories
  'PRODUCT_CATEGORIES.HEADING_TITLE': 'Loại sản phẩm',
  'PRODUCT_CATEGORIES.PARENT': 'Danh mục cha',

  // Product Brands
  'PRODUCT_BRANDS.HEADING_TITLE': 'Nhãn hiệu',

  // Products
  'PRODUCTS.HEADING_TITLE': 'Sản phẩm',
  'PRODUCTS.INCLUDE': 'Bộ sản phẩm bao gồm',

  // Modules
  'MODULES.HEADING_TITLE': 'Mô-đun',

  // Orders
  'ORDERS.HEADING_TITLE': 'Đơn hàng',
  'ORDERS.STATE': 'Tình trạng',
  'ORDERS.PAYMENT_STATUS': 'Thanh toán',

  // Order detail
  'ORDER_DETAIL.TITLE': 'Chi tiết đơn hàng',
  'ORDER_DETAIL.NAME': 'Khách hàng',
  'ORDER_DETAIL.PHONE': 'Số điện thoại',
  'ORDER_DETAIL.EMAIL': 'Email',
  'ORDER_DETAIL.SHIPPING_ADDRESS': 'Địa chỉ giao hàng',
  'ORDER_DETAIL.NOTE': 'Ghi chú',
  'ORDER_DETAIL.STATE': 'Tình trạng đơn hàng',
  'ORDER_DETAIL.PAYMENT_STATUS': 'Tình trạng thanh toán',
  'ORDER_DETAIL.PRODUCT': 'Sản phầm',
  'ORDER_DETAIL.QUANTITY': 'Số lượng',
  'ORDER_DETAIL.PRICE': 'Đơn giá',
  'ORDER_DETAIL.SUBTOTAL': 'Thành tiền',
  'ORDER_DETAIL.AMOUNT': 'Tổng tiền',
  'ORDER_DETAIL.CREATE_DATE': 'Ngày tạo',

  // Pages
  'PAGES.HEADING_TITLE': 'Trang',

  // Page edit
  'PAGE_EDIT.LAYOUT': 'Layout',
  'PAGE_EDIT.SHOW_FEATURED_ONLY': 'Chỉ hiện sản phẩm nổi bật',
  'PAGE_EDIT.PRODUCT_ORDER': 'Sắp xếp',
  'PAGE_EDIT.PRODUCT_ORDER_NEWEST': 'Sản phẩm mới nhất',
  'PAGE_EDIT.PRODUCT_ORDER_RANDOM': 'Sản phẩm ngẫu nhiên',
  'PAGE_EDIT.PRODUCTS_LIMIT': 'Số lượng sản phẩm',
  'PAGE_EDIT.PRODUCTS_CAPTION': 'Caption',
  'PAGE_EDIT.CONTACT.ADDRESS_HINT': 'Địa chỉ hiển thị trên google map'
}
