import { en, vi } from '@/translate'

export default {
  /**
   * Set user data
   * @param {object} state State
   * @param {object} user  User object
   */
  SET_USER (state, user) {
    state.user = user
  },

  /**
   * Set token
   * @param {object} state State
   * @param {string} token Token string
   */
  SET_TOKEN (state, token) {
    state.token = token

    if (token === null) {
      window.localStorage.removeItem('token')
    } else {
      window.localStorage.setItem('token', token)
    }
  },

  /**
   * Set translate
   * @param {object} state  State
   * @param {string} locale Locale string
   */
  SET_TRANSLATE (state, locale) {
    switch (locale) {
      case 'en':
        state.translate = en
        break

      default:
        state.translate = vi
        break
    }
  }
}
