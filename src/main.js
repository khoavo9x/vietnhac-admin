// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import router from './router'
import components from './components'
import store from './store'
import GlobalMixin from './mixins/global'
import config from './config'
import { moneyFormat } from './filters'

Vue.use(Vuetify)
Vue.config.productionTip = false

// Register custom common components
Object.keys(components).forEach(key => {
  Vue.component(key, components[key])
})

// Register global mixin
Vue.mixin(GlobalMixin)

// Register filter
Vue.filter('moneyFormat', moneyFormat)

// Handle authorize guard
router.beforeEach((to, from, next) => {
  // Redirect to dashboard if user is logged in by default
  if (to.path === '/login' && store.state.user !== null) {
    next({
      path: config.defaultRoute
    })

    return
  }

  // Redirect to login if user still not logged in by default
  if (to.path !== '/login' && store.state.user === null) {
    let nextRoute = { path: '/login' }

    if (to.fullPath !== '/') {
      nextRoute.query = { redirect: to.fullPath }
    }

    next(nextRoute)

    return
  }

  next()
})

// Set translation
store.commit('SET_TRANSLATE', 'vi')

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store: store,
  template: '<App/>',
  components: { App }
})
