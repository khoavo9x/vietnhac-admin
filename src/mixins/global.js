import config from '@/config'
import api from '@/api'
import { EventBus } from '@/eventbus'
import { mapState } from 'vuex'

export default {
  computed: {
    ...mapState({
      TRANSLATE: 'translate'
    })
  },

  data () {
    return {
      /**
       * server uri
       * @type {string}
       */
      SERVERURI: config.serverURI,

      /**
       * api helper object
       * @type {Object}
       */
      API: api,

      /**
       * EventBus object
       * @type {Object}
       */
      EVENTBUS: EventBus
    }
  },

  methods: {
    asset (src) {
      if (src === '') {
        src = 'images/default.jpg'
      }

      return this.SERVERURI + '/' + src
    },

    logout () {
      // Clear local storage
      window.localStorage.removeItem('token')

      // Redirect
      window.location.href = config.assetsPublicPath
    }
  }
}
