import axios from 'axios'
import config from '@/config'
import { EventBus } from '@/eventbus'

export default {
  /**
   * Request data
   *
   * @param  {object} args Arguments object. Include:
   *                       {string}   method
   *                       {string}   uri
   *                       {object}   headers
   *                       {array}    transformRequest
   *                       {object}   data
   *                       {object}   params
   *                       {function} success
   *                       {function} error
   *
   * @return {bool}
   */
  request (args) {
    // Check mandatory arguments
    if (args.method === undefined || args.uri === undefined) {
      console.log('API function call require "method" and "uri" in arguments object!')

      return false
    }

    // Method
    const method = args.method

    // Url
    const url = config.serverURI + config.apiURI + args.uri

    // Prepare headers
    let headers = {
      'X-Requested-With': 'XMLHttpRequest',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + window.localStorage.getItem('token')
    }

    if (typeof args.headers === 'object') {
      headers = Object.assign(headers, args.headers)
    }

    // Prepare data
    let data = {}

    if (typeof args.data === 'object') {
      data = Object.assign(data, args.data)
    }

    // Prepare params
    let params = {}

    if (typeof args.params === 'object') {
      params = Object.assign(params, args.params)
    }

    // Prepare transformRequest
    let transformRequest = null

    if (typeof args.transformRequest === 'object') {
      transformRequest = args.transformRequest
    }

    // Get request
    axios({method, url, headers, transformRequest, data, params})
    .then(response => {
      if (typeof args.success === 'function') {
        args.success(response)
      }
    })
    .catch(error => {
      if (typeof args.error === 'function') {
        args.error(error)
      } else if (error.response.status === 401) {
        EventBus.$emit('tokenisout')
      }
    })

    return true
  },

  /**
   * Api for login
   *
   * @param  {object} auth  Authorization data include username and password
   *
   * @return {axios}
   */
  getToken (auth) {
    if (!auth) {
      console.log('API getToken call require auth argument')
    }

    if (!auth.username || !auth.password) {
      return false
    }

    return axios({
      method: 'post',
      url: config.serverURI + config.tokenURI,
      headers: {
        'Accept': 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
      },
      data: {
        'grant_type': 'password',
        'client_id': config.client_id,
        'client_secret': config.client_secret,
        'username': auth.username,
        'password': auth.password,
        'scope': '*'
      }
    })
  }
}
