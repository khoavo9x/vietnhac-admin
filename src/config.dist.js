export default {
  serverURI: 'http://localhost',
  apiURI: '/api',
  tokenURI: '/oauth/token',
  client_id: 1,
  client_secret: '',
  assetsPublicPath: '/',
  defaultRoute: '/dashboard'
}
