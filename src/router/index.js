import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/layouts/Login'
import Main from '@/layouts/Main/index'
import Dashboard from '@/layouts/Dashboard/index'
import ProductCategories from '@/layouts/ProductCategories/index'
import ProductBrands from '@/layouts/ProductBrands/index'
import Products from '@/layouts/Products/index'
import Modules from '@/layouts/Modules/index'
import Orders from '@/layouts/Orders/index'
import Settings from '@/layouts/Settings/index'
import Pages from '@/layouts/Pages/index'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/',
      component: Main,
      children: [
        {
          path: '/dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: '/productcategories',
          name: 'ProductCategories',
          component: ProductCategories
        },
        {
          path: '/productbrands',
          name: 'ProductBrands',
          component: ProductBrands
        },
        {
          path: '/products',
          name: 'Products',
          component: Products
        },
        {
          path: '/modules',
          name: 'Modules',
          component: Modules
        },
        {
          path: '/orders',
          name: 'Orders',
          component: Orders
        },
        {
          path: '/settings',
          name: 'Settings',
          component: Settings
        },
        {
          path: '/pages',
          name: 'Pages',
          component: Pages
        }
      ]
    }
  ]
})
